local DEBUG = false

local deadzone = 0.0
local base = 35.0
local powerAdjust = 1.0
local torqueAdjust = 1.0
local angleImpact = 3.0
local speedImpact = 2.0

Citizen.CreateThread(function ()
  local function PowerLoop()
    local player = PlayerId()
    local playerPed = PlayerPedId()

    -- check if player ped exists and control is on (e.g. not in a cutscene)
    if not DoesEntityExist(playerPed) or not IsPlayerControlOn(player) then
      return
    end

    -- check for player ped death and player arrest
    if IsEntityDead(playerPed) or IsPlayerBeingArrested(player, true) then
      return
    end

    local vehicle = GetVehiclePedIsUsing(playerPed)
  	if not IsThisModelACar(GetEntityModel(vehicle)) then
  		return
    end

    if IsVehicleStopped(vehicle) then
      return
    end

    if base < 0 then
      base = 35.0
    end

    local speed = GetEntitySpeed(vehicle)
    local vec = GetEntitySpeedVector(vehicle, true)
    local angle = math.acos(vec.y / speed) * 180.0 / math.pi
    local speedMultiplier = 0.0

    if speed < base then
      speedMultiplier = (base - speed) / base
    end

    local powerMultiplier = 1.0 + powerAdjust * (((angle / 90) * angleImpact) + ((angle / 90) * speedMultiplier * speedImpact))
    local torqueMultiplier = 1.0 + torqueAdjust * (((angle / 90) * angleImpact) + ((angle / 90) * speedMultiplier * speedImpact))

    -- works on keyboard and controller. param 1, don't know what it does
  	-- pressed/max = 254. default/depressed = 127.
  	local accelValue = GetControlValue(0, 71);
  	local brakeValue = GetControlValue(0, 72);

    if not (angle > 135 and brakeValue > accelValue + 12) and angle > deadzone then
      SetVehicleEnginePowerMultiplier(vehicle, powerMultiplier)
			SetVehicleEngineTorqueMultiplier(vehicle, torqueMultiplier)
    end

    if DEBUG then
      local pos = GetEntityCoords(vehicle);
      local _, x, y = GetScreenCoordFromWorldCoord(pos.x, pos.y, pos.z)
  		local fmt = 'X %.02f\nY %.02f\nVel %.02f\nPowX %.02f\nTorX %.02f\nAngle %.02f'
      local text = string.format(fmt, vec.x, vec.y, speed, powerMultiplier, torqueMultiplier, angle)
      
  		SetTextFont(0)
  		SetTextScale(0.2, 0.2)
  		SetTextColour(255, 255, 255, 255)
  		SetTextWrap(0.0, 1.0)
  		SetTextCentre(0)
  		SetTextDropshadow(0, 0, 0, 0, 0)
  		SetTextEdge(1, 0, 0, 0, 205)
  		BeginTextCommandDisplayText('STRING')
  		AddTextComponentSubstringPlayerName(text)
  		EndTextCommandDisplayText(x, y)
  		DrawRect(x + 0.027, y + 0.043, 0.058, 0.096, 75, 75, 75, 75)
    end
  end

  while true do
    Citizen.Wait(0)
    PowerLoop()
  end
end)
